#!/usr/bin/env ruby

require_relative './app/salt'

salt = Salt.new
salt.gitlab '13.5.4'

# Load a license for 10 users
salt.add_license 'my-10-user-license.gitlab-license'

# Create 10 users
# There are 11 users, the 10 we created + root
salt.create_users '01'..'10'

# Make a screenshot of the Admin panel
salt.screenshot 'http://localhost/admin', name: 'screen 1'

# Run the historical maximum user counter task
salt.kick_muc

# Make a screenshot of the Admin panel
salt.screenshot 'http://localhost/admin', name: 'screen 2'

# Delete one user
salt.delete_user '10'

# Run the historical maximum user counter task
salt.kick_muc

# Make a screenshot of the Admin panel
salt.screenshot 'http://localhost/admin', name: 'screen 3'

# Stop & delete the Docker container
salt.stop
salt.delete
