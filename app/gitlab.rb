require_relative 'gitlab_user'
require_relative 'gitlab_license'
require_relative 'gitlab_vm'

class GitLab

  def initialize(version, quick = false)
    @quick = quick
    # Fails for the odd '-ee.1' release...
    @machine = Machine.new image_name(version), @quick
    @api = Api.new
    unless @quick
      wait_for_gitlab_running
      generate_pat
      disable_signup
    end
  end

  def image_name(version)
    # Almost all images since version 9 were released with name gitlab/gitlab-ee:#{version}-ee.0
    # Exception: 13.3.0-ee.1
    dotversion = 0
    if version == '13.3.0'
      dotversion = 1
    end
    "gitlab/gitlab-ee:#{version}-ee.#{dotversion}"
  end

  def stop
    @machine.stop_container
  end

  def delete
    @machine.delete_container
  end

  def delete_image
    @machine.delete_image
  end

  def wait_for_gitlab_running
    print '> '
    loop do
      status, data = @api.get 'http://localhost/users/sign_in'
      if [200, 302].include? status
        puts '*'
        break
      elsif status.nil?
        print '_'
      else
        print '~'
      end
      sleep 5
    end
  end

  def generate_pat
    puts '> 🔧 Generate PAT...'
    command = 'root = User.find(1); \
               token = root.personal_access_tokens.create(scopes: [:api], name: "Salt"); \
               token.set_token("salt-automation-token"); \
               token.save!'
    rails_runner command
  end

  def disable_signup
    puts '> 🔧 Disable signup'
    status, data = @api.put 'application/settings?signup_enabled=false'
    # 200 = OK
    puts "⛔ Failed: #{status}" unless status == 200
  end

  def rails_runner(command)
    stdout = `docker container exec -it #{@machine.name} \
              gitlab-rails runner '#{command}'`
  end

  def rails_runner_script(script)
    stdout = `docker container exec -it #{@machine.name} \
              gitlab-rails runner #{script}`
  end

  def docker_command(command)
    stdout = `docker container exec -it #{@machine.name} #{command}`
  end

  def docker_copy_file(source, destination)
    stdout = `docker container cp #{source} #{@machine.name}:#{destination}`
  end

end
