class Salt

  def license_info
    puts '> 🎫 Get license information'
    valid, status, data = @gitlab.license_info
    if valid
      data.each { |k, v| puts "#{k} = #{v}" }
      # p data
    else
      puts "⛔️ Failed: #{status} - #{data}"
    end
  end

  def add_license filename
    puts "> 🎫 Add license #{filename}"
    valid, status, data = @gitlab.add_license filename
    puts "⛔ Failed: #{status} - #{data}" unless valid
  end

end
