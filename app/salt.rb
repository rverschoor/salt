require 'docker'
require 'faraday'
require 'json'
require 'securerandom'
require 'open3'
require 'ferrum'
require_relative 'salt_user'
require_relative 'salt_license'
require_relative 'salt_gitlab'
require_relative 'salt_time_travel'
require_relative 'salt_screenshot'
require_relative 'salt_iisi'
require_relative 'api'
require_relative 'machine'
require_relative 'gitlab'

class Salt

  def initialize(args = nil)
    @quick = args == :quick
  end

  def gitlab(version)
    puts "> 🏁 Starting GitLab #{version}"
    @gitlab = GitLab.new version, @quick
    @vm = @gitlab.test_vm
  end

  def stop
    @gitlab.stop
  end

  def delete
    @gitlab.stop
    @gitlab.delete
    @gitlab.delete_image
    @browser&.quit
  end

  def bye
    delete
    time_machine_off
  end

  def bye_keep_image
    @gitlab.stop
    @gitlab.delete
    time_machine_off
    @browser&.quit
  end

  def pause
    puts '> ⏸️ Pause, hit Return to continue'
    $stdin.gets
  end

end
