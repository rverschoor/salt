class Salt

  def show_details_users
    puts '> 👨‍👨‍👧‍👦 Show details of all users'
    valid, status, data = @gitlab.show_details_users
    if valid
      data.each { |r| r.each { |k, v| puts "#{k} = #{v}" }; puts }
    else
      puts "⛔ Failed: #{status}"
    end
  end

  def show_details_user name
    puts "> 👀 Show details of user #{name}"
    valid, status, data = @gitlab.show_details_user name
    if valid
      data.each { |k, v| puts "#{k} = #{v}" }
    else
      puts "⛔ Failed: #{status}"
    end
  end

  def create_user name
    puts "> 👶 Create user #{name}"
    valid, status, data = @gitlab.create_user name
    puts "⛔️ Failed: #{status} - #{data['message'] if data}" unless valid
  end

  def create_users range
    range.each { |u| create_user u }
  end

  def block_user name
    puts "> 🧟 Block user #{name}"
    valid, status, data = @gitlab.block_user name
    puts "⛔️ Failed: #{status} - #{data}" unless valid
  end

  def block_users range
    range.each { |u| block_user u }
  end

  def unblock_user name
    puts "> 🧑 Unblock user #{name}"
    valid, status, data = @gitlab.unblock_user name
    puts "⛔ Failed: #{status} - #{data}" unless valid
  end

  def unblock_users range
    range.each { |u| unblock_user u }
  end

  def deactivate_user name
    puts "> 🧟 Deactivate user #{name}"
    valid, status, data = @gitlab.deactivate_user name
    puts "⛔️ Failed: #{status} - #{data}" unless valid
  end

  def deactivate_users range
    range.each { |u| deactivate_user u }
  end

  def activate_user name
    puts "> 🧑 Activate user #{name}"
    valid, status, data = @gitlab.activate_user name
    puts "⛔️ Failed: #{status} - #{data}" unless valid
  end

  def activate_users range
    range.each { |u| activate_user u }
  end

  def delete_user name
    puts "> 💀 Delete user #{name}"
    valid, status, data = @gitlab.delete_user name
    puts "⛔️ Failed: #{status} - #{data}" unless valid
  end

  def delete_users range
    range.each { |u| delete_user u }
  end

end
