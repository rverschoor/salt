class Machine

  def initialize(image, quick = false)
    @quick = quick
    @root_password = 'password'
    @container_name = 'salt'
    @image_name = image
    unless @quick
      create_image
      remove_container
      create_container
      start_container
    end
  end

  def name
    @container_name
  end

  def create_image
    @image = Docker::Image.create(fromImage: @image_name, opts: { retries: 3 })
  end

  def remove_container
    # If a container already exists with the same name, the container create will barf.
    # Docker-api doesn't seem to have a method to delete a container by name.
    # Workaround: check if the container by that name exists and delete using docker CLI...
    list = Docker::Container.all(all: true, filters: { name: [@container_name] }.to_json)
    `docker container rm -f #{@container_name}` if list.length.positive?
  end

  def create_container
    omnibus_config = "GITLAB_OMNIBUS_CONFIG=gitlab_rails['initial_root_password'] = '#{@root_password}'"
    @container = Docker::Container.create(
      'name' => @container_name,
      'Image' => @image_name,
      'Env' => [omnibus_config],
      'ExposedPorts' => { '80/tcp' => {} },
      'HostConfig' => {
        'PortBindings' => {
          '80/tcp' => [{ 'HostPort' => '80' }]
        }
      }
    )
  end

  def start_container
    @container.start
  end

  def stop_container
    @container&.stop
  end

  def delete_container
    @container&.delete
  end

  def delete_image
    @image.remove
  end

end
