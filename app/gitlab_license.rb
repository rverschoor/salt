class GitLab

  def license_info
    status, data = @api.get 'license'
    # 200 = OK
    valid = (status == 200)
    if valid
      data ||= 'No license loaded'
    else
      data = "⛔️ Failed: #{status}"
    end
    [valid, status, data]
  end

  def add_license(filename)
    begin
      license = File.read filename
    rescue StandardError => e
      status = 666
      data = e.message
    else
      status, data = @api.post "license?license=#{license}"
      # 201 = OK, 400 = NOK
    end
    valid = (status == 201)
    [valid, status, data]
  end

end
