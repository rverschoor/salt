class Salt

  def check_if_time_travel_is_safe
    unless @vm
      puts '⚠️ Time machine can only be used in a VM'
      exit
    end
  end

  def time_machine stamp
    puts "> ⏳ Travel in time to #{stamp}"
    check_if_time_travel_is_safe
    `timedatectl set-ntp false`
    # set-ntp is asynchronous; wait until timesyncd stops before the time can be set
    `while systemctl is-active systemd-timesyncd; do sleep 1; done`
    `timedatectl set-time "#{stamp}"`
    @time_machine_running = true
  end

  def time_machine_off
    return unless @time_machine_running

    puts '> ⌛ Switch off time machine'
    check_if_time_travel_is_safe
    `timedatectl set-ntp true`
    @time_machine_running = false
  end

end
