class GitLab

  def test_vm
    test_dmesg= test_vm_dmesg
    test_cpuinfo = test_vm_cpuinfo
    test_chassis = test_vm_chassis
    (test_dmesg and test_cpuinfo and test_chassis)
  end

  def test_vm_dmesg
    # $ dmesg |grep 'Hypervisor detected'
    # Output: VM: [    0.000000] Hypervisor detected: KVM
    #
    # Linux non-root: runs without sudo
    # Physical: no output
    # OSX non-root: Unable to obtain kernel buffer: Operation not permitted
    #               usage: sudo dmesg
    is_vm = false
    stdout, _stderr, _status = Open3.capture3('dmesg |grep "Hypervisor detected"')
    is_vm = true if stdout.length && (stdout.include? 'Hypervisor detected')
    is_vm
  end

  def test_vm_cpuinfo
    # $ cat /proc/cpuinfo | grep flags | grep hypervisor
    # Output: long text with all flags
    # Linux non-root: runs without sudo
    # Physical: no output
    # OSX non-root: cat: /proc/cpuinfo: No such file or directory
    is_vm = false
    stdout, _stderr, _status = Open3.capture3('cat /proc/cpuinfo | grep flags | grep hypervisor')
    is_vm = true if stdout.length && (stdout.downcase.include? 'hypervisor')
    is_vm
  end

  def test_vm_chassis
    # $ hostnamectl | grep 'Chassis: vm'
    # Output: Chassis: vm
    #
    # Linux non-root: runs without sudo
    # Physical: no output
    # OSX non-root: zsh: command not found: hostnamectl
    is_vm = false
    stdout, _stderr, _status = Open3.capture3('hostnamectl | grep "Chassis: vm"')
    is_vm = true if stdout.length && stdout.chomp.strip == 'Chassis: vm'
    is_vm
  end

end