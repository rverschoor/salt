class Salt

  def rails_runner(command)
    puts "> 🏃 Run gitlab-rails runner '#{command}'"
    stdout = @gitlab.rails_runner command
    puts stdout
  end

  def _kick_counter(message, command)
    puts "> 🔄 #{message}"
    stdout = @gitlab.rails_runner command
  end

  def kick_muc_command
    'p ::HistoricalDataWorker.new.perform;'
  end

  def kick_buc_command
    command = 'identifier = Analytics::UsageTrends::Measurement.identifiers[:billable_users];'
    command += '::Analytics::UsageTrends::CounterJobWorker.new.perform(identifier, User.minimum(:id), User.maximum(:id), Time.zone.now);'
    command
  end

  def kick_muc
    _kick_counter('Kick historical max user counter', kick_muc_command)
  end

  def kick_buc
    _kick_counter('Kick billable users counter', kick_buc_command)
  end

  def kick_all
    _kick_counter('Kick all counters', kick_muc_command + kick_buc_command)
  end

end
