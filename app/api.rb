class Api

  def initialize
    retry_options = {
      max: 3,
      interval: 1,
      interval_randomness: 0.5,
      backoff_factor: 2,
      # methods: %i[delete, get, head, options, put, post], # Add post. Not idempotent, but often results in 502
      retry_if: lambda { |env, _exc|
        p env
        true
      },
      retry_statuses: [502]
    }
    @api = Faraday.new(url: 'http://localhost/api/v4', headers: { 'Private-Token' => 'salt-automation-token' })
    @api.request(:retry, retry_options)
  end

  def get(endpoint)
    response = @api.get endpoint
  rescue StandardError => e
    [nil, nil]
  else
    begin
      json = JSON.parse response.body
    rescue JSON::ParserError
      json = nil
    end
    # sleep 1
    [response.status, json]
  end

  def post(endpoint, payload = nil)
    response = @api.post endpoint, payload
  rescue StandardError => e
    [nil, nil]
  else
    begin
      json = JSON.parse response.body
    rescue JSON::ParserError
      json = nil
    end
    # sleep 1
    [response.status, json]
  end

  def put(endpoint, payload = nil)
    response = @api.put endpoint, payload
  rescue StandardError => e
    [nil, nil]
  else
    begin
      json = JSON.parse response.body
    rescue JSON::ParserError
      json = nil
    end
    # sleep 1
    [response.status, json]
  end

  def delete(endpoint)
    response = @api.delete endpoint
  rescue StandardError => e
    nil
  else
    # sleep 1
    response.status
  end

end
