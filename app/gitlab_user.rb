class GitLab

  def username_to_id(name)
    status, data = @api.get "users?username=#{name}"
    # If username is not found, status=200 and data=[]
    id = if (status == 200) && (data.length.positive?)
           data[0]['id']
         else
           nil
         end
    [status, id]
  end

  def show_details_users
    status, data = @api.get 'users'
    # 200 = OK
    valid = (status == 200)
    [valid, status, data]
  end

  def show_details_user(name)
    status, id = username_to_id name
    if id
      status, data = @api.get "users/#{id}"
      # 200 = OK
      valid = (status == 200)
    else
      valid = false
      data = 'User not found'
    end
    [valid, status, data]
  end

  def create_user(name)
    fullname = "#{name}"
    username = "#{name}"
    email = "#{name}@example.com"
    password = SecureRandom.alphanumeric(30)
    status, data = @api.post 'users', {name: fullname, username: username, email: email, password: password}
    # 201 = OK, 409 = Conflict, email already taken
    valid = (status == 201)
    [valid, status, data]
  end

  def delete_user(name)
    status, id = username_to_id name
    if id
      status = @api.delete "users/#{id}"
      # 204 = OK, 409 = Can't be soft deleted
      valid = (status == 204)
    else
      valid = false
      data = 'User not found'
    end
    [valid, status, data]
  end

  def block_user(name)
    status, id = username_to_id name
    if id
      status, data = @api.post "users/#{id}/block"
      # 201 = OK, 403 = Forbidden
      valid = (status == 201)
    else
      valid = false
      data = 'User not found'
    end
    [valid, status, data]
  end

  def unblock_user(name)
    status, id = username_to_id name
    if id
      status, data = @api.post "users/#{id}/unblock"
      # 201 = OK, 403 = Forbidden
      valid = (status == 201)
    else
      valid = false
      data = 'User not found'
    end
    [valid, status, data]
  end

  def deactivate_user(name)
    status, id = username_to_id name
    if id
      status, data = @api.post "users/#{id}/deactivate"
      # 201 = OK, 403 = Forbidden
      valid = (status == 201)
    else
      valid = false
      data = 'User not found'
    end
    [valid, status, data]
  end

  def activate_user(name)
    status, id = username_to_id name
    if id
      status, data = @api.post "users/#{id}/activate"
      # 201 = OK, 403 = Forbidden
      valid = (status == 201)
    else
      valid = false
      data = 'User not found'
    end
    [valid, status, data]
  end

end
