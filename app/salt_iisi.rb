class Salt

  def _download_iisi
    `wget -q -O iisi.rb https://gitlab.com/rverschoor/iisi/-/raw/main/iisi.rb`
    @gitlab.docker_copy_file 'iisi.rb', '/iisi.rb'
    @gitlab.docker_command 'chmod +x /iisi.rb'
    @iisi_downloaded = true
  end

  def iisi(options = '--summary')
    _download_iisi unless @iisi_downloaded
    puts "> 🧭 Run iisi script #{options}"
    puts @gitlab.rails_runner_script "/iisi.rb #{options}"
  end

end
