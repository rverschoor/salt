class Salt

  def screenshot(url, name: nil)
    name ||= Time.now.utc.strftime('%Y%m%d-%H%M%S')
    puts "> 🎬 Make screenshot of #{url} as #{name}.png"
    make_screenshot url, name
  end

  def make_screenshot(url, name)
    _create_browser unless @browser
    if _goto_page url
      @browser.screenshot(path: "#{name}.png", full: true)
    else
      @browser.screenshot(path: "#{name}-error.png", full: true)
    end
  end

  def _goto_page(url)
    @browser.go_to url
    _reset_password if @browser.current_url.start_with?('http://localhost/users/password/edit?reset_password_token=')
    _sign_in if @browser.current_url == 'http://localhost/users/sign_in'
    @browser.go_to url if @browser.current_url == 'http://localhost'
    sleep 5
    if @browser.current_url != url
      puts "⛔️ at #{@browser.current_url} instead of screenshot target #{url}"
      return false
    end
    true
  end

  def _create_browser
    @browser = Ferrum::Browser.new(
      browser_options: {
        'no-sandbox': nil,
        'ignore-certificate-errors': nil,
        'disable-popup-blocking': nil,
        'disable-translate': nil,
        'disable-notifications': nil,
        'start-maximized': nil,
        'disable-gpu': nil,
        'headless': nil
      }
    )
  end

  def _reset_password
    begin
      @browser.at_xpath("//input[@id='user_password']").focus.type('password')
      @browser.at_xpath("//input[@id='user_password_confirmation']").focus.type('password')
      @browser.at_xpath("//input[@type='submit']").click
    rescue StandardError => e
      puts "⛔️ #{e.class}: #{e.message}"
    end
    sleep 3
  end

  def _sign_in
    begin
      @browser.at_xpath("//input[@id='user_login']").focus.type('root')
      @browser.at_xpath("//input[@id='user_password']").focus.type('password')
      @browser.at_xpath("//input[@type='submit']")&.click
      @browser.at_xpath("//button[@type='submit']")&.click
    rescue StandardError => e
      puts "⛔️ #{e.class}: #{e.message}"
    end
    sleep 3
  end

end
