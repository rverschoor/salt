# Semi Automatic License Tester (SALT)

## Info
SALT can be used to test GitLab licenses. \
What happens to the user counts on the Admin panel when you add users to a GitLab instance and you exceed the license? \
What happens when you try to load a license which doesn't have enough true-ups? \
What happens when you reduce the number of users trying to make the new license work? \
You can now write a simple Ruby script which uses SALT's functionality to reproduce these scenarios.

## Environment
SALT should run in Linux and OSX, but it's not thoroughly tested out in the wild. \
I didn't bother to look at Windows. \
If you want to time travel you **must** run SALT in a Linux VM. \
Trying to time travel on a physical machine will mess up your system, so SALT does its best to prevent that.

## Installation

### OSX
I developed SALT on a long running OSX, so I have no clue what extras need to be installed for SALT (if any).
Ruby and Docker Engine must be installed, and Chrome is needed for the screenshots.

### Linux
Starting with a standard Ubuntu 18.04 server:

- Bring system up-to-date
  - `apt update`
  - `apt upgrade -y`
- Install Ruby and Bundler 2
  - `apt install ruby -y`
  - `gem install --no-document bundler`
- Install Docker Engine
  - `curl -fsSL https://get.docker.com -o get-docker.sh`
  - `sh get-docker.sh`
- Install various stuff for gems and Chrome
  - `apt install ubuntu-dev-tools zlib1g-dev imagemagick -y`
  - `apt install libappindicator1 fonts-liberation ruby-dev -y`
  - `wget https://dl.google.com/linux/direct/google-chrome-stable_current_amd64.deb`
  - `dpkg -i google-chrome*.deb` \
  Note: this will throw an error, which you can ignore:
    ```
    dpkg: error processing package google-chrome-stable (--install):
     dependency problems - leaving unconfigured
    Processing triggers for mime-support (3.60ubuntu1) ...
    Processing triggers for man-db (2.8.3-2ubuntu0.1) ...
    Errors were encountered while processing:
     google-chrome-stable
    ```
  - `apt --fix-broken install -y`
- Get SALT
  - `git clone https://gitlab.com/rverschoor/salt.git`
- Install bundler and gems used by SALT
  - `cd salt/app`
  - `gem install bundler -v "$(grep -A 1 "BUNDLED WITH" Gemfile.lock | tail -n 1)"`  
  - `bundle install`
  - `cd ..`

Run the example script to test SALT:
- `./example.rb`

## Security
If you are running SALT in a publicly accessible VM, you must be aware of some weaknesses. \
The GitLab instance that is started in a Docker container is currently not very secure.

The good:

- It has user signup disabled
- Users are created with a random password

The bad:

- It can be accessed on port 80 
- Root account is currently created with password `password`
- PAT is currently hardcoded

Because of these low security barriers there is a risk an outsider visits your instance and grabs your license/s. \
It's a good habit to delete the GitLab container once you're done testing (SALT can do that for you).

## Usage
To use SALT you write a Ruby script. \
You need to include SALT and create a SALT object:

```ruby
# Include SALT
require './app/salt'

# Initialize SALT
salt = Salt.new
```

Once you have your SALT object you can run its commands.

## Commands 

### `Specifying multiple users`
Some commands act upon multiple users. \
You can specify the usernames with a Ruby range or array. \
Some examples:
```ruby
# Users 1, 2, 3
1..3

# Users 01, 02, and 03
'01'..'03'

# Users a, b, and c
'a'..'c'

# Users user-01, user-02, and user-03
'user-01'..'user-03'

# Users 10, 20, and 31
[10, 20, 31]

# Users aap, noot, and mies
['aap', 'noot', 'mies']

# Users 13, '007`, and `Jhon`
[13, '007', 'Jhon']
```

### `gitlab`
Start a GitLab instance in a Docker container. \
The name of the container will be `salt`. \
If an old `salt` container was still running, it will be replaced with a new fresh one. \
The script will continue once it's running and responsive. \
You can access it at `http://localhost` and log in with username `root` and password `password`.
NB This command takes some time to complete.

Arguments:

- GitLab version (string, _required_)

```ruby
salt.gitlab '13.5.4'
```

### `stop`
Stop the `salt` container with the GitLab instance. \
You are not required to use this in your script.

Arguments:

- none

```ruby
salt.stop
```

### `delete`
Delete the `salt` container with the GitLab instance. \
It will also delete the Docker image, to prevent your disk from filling up. \
You are not required to use this in your script.

Arguments:

- none

```ruby
salt.delete
```

### `bye`
Convenience wrapper for `stop`, `delete`, plus `time_machine_off`. \
A complete cleanup, except for screenshots you made.

Arguments:

- none

```ruby
salt.bye
```

### `bye_keep_image`
Same as `bye`, but it won't delete the Docker image.

Arguments:

- none

```ruby
salt.bye_keep_image
```

### `create_user`
Create a user. \
Given a <username\>, it will create an account with:

- username = "<username\>"
- fullname = "<username\>"
- email = "<username\>@example.com"
- password = random

Arguments:

- **username**: string or integer (_required_)

```ruby
salt.create_user 12
salt.create_user '013'
salt.create_user 'Bandersnatch'
```

### `create_users`
Create multiple users.

Arguments:

- **usernames**: string, integer, array or range of strings or integers (_required_)

```ruby
# Create users 10, 11, and 12
salt.create_users 10..12
```

### `delete_user`
Delete user <username\>.

Arguments:

- **username**: string or integer (_required_)

```ruby
salt.delete_user 12
salt.delete_user '013'
salt.delete_user 'Bandersnatch'
```

### `delete_users`
Delete multiple users.

Arguments:

- **usernames**: string, integer, array or range of strings or integers (_required_)

```ruby
salt.delete_users [12, '013', 'Bandersnatch']
```

### `block_user`
Block user <username\>.

Arguments:

- **username**: string or integer (_required_)

```ruby
salt.block_user 666
salt.block_user '007'
salt.block_user 'Jabberwocky'
```

### `block_users`
Block multiple users.

Arguments:

- **usernames**: string, integer, array or range of strings or integers (_required_)

```ruby
salt.block_users [666, '007', 'Jabberwocky']
```

### `unblock_user`
Unblock user <username\>.

Arguments:

- **username**: string or integer (_required_)

```ruby
salt.unblock_user 666
salt.unblock_user '007'
salt.unblock_user 'Jabberwocky'
```

### `unblock_users`
Unblock multiple users.

Arguments:

- **usernames**: string, integer, array or range of strings or integers (_required_)

```ruby
salt.unblock_users [666, '007', 'Jabberwocky']
```

### `deactivate_user`
Deactivate user <username\>.

Arguments:

- **username**: string or integer (_required_)

```ruby
salt.deactivate_user 42
salt.deactivate_user '4711'
salt.deactivate_user 'Blixa'
```

### `deactivate_users`
Deactivate multiple users.

Arguments:

- **usernames**: string, integer, array or range of strings or integers (_required_)

```ruby
salt.deactivate_users [42, '4711', 'Blixa']
```

### `activate_user`
Activate user <username\>.

Arguments:

- **username**: string or integer (_required_)

```ruby
salt.activate_user 42
salt.activate_user '4711'
salt.activate_user 'Blixa'

```
### `activate_users`
Activate multiple users.

Arguments:

- **usernames**: string, integer, array or range of strings or integers (_required_)

```ruby
salt.activate_users [42, '4711', 'Blixa']
```

### `show_details_user`
Shows information of a user.

Arguments:

- **usernames**: string, integer, array or range of strings or integers (_required_)

```ruby
salt.show_details_user 'Blixa'
```

### `show_details_users`
Shows information of all users.

Arguments:

- none

```ruby
salt.show_details_users
```

### `add_license`
Load a license.

Arguments:

- **filename**: string (_required_) \

```ruby
salt.add_license 'test license starter-20 20201101.gitlab-license'
```

### `license_info`
Show information about all loaded licenses.

Arguments:

- none

```ruby
salt.license_info
```

### `kick_muc `
Run the historical maximum user counter task. \
This task normally runs at midnight UTC. \
It updates the historical maximum user count in the database. \
This is the value displayed as `Maximum User Count` in the Admin page and used to determine `Users over License`. \
NB This command takes some time to complete.

Arguments:

- none

```ruby
salt.kick_muc
```

### `kick_buc `
Run the billable users counter task. \
This task normally runs at 23:50 UTC. \
It updates the billable users count in the database. \
This is the value displayed as `Billable users` in the Admin page. \
NB This command takes some time to complete.

Arguments:

- none

```ruby
salt.kick_buc
```

### `kick_all `
Runs both `kick_muc` and `kick_buc`.

Arguments:

- none

```ruby
salt.kick_all
```

### `rails_runner`
Run a `gitlab-rails runner` command. \
This can be used to run the typical commands we ask the customers to run. \
NB This command takes some time to complete.

Arguments:

- **command**: string (_required_)

```ruby
salt.rails_runner 'p User.active.count'
```

You can combine multiple commands in one call:

```ruby
salt.rails_runner 'p User.active.count; p ::HistoricalData.max_historical_user_count'
```
### `screenshot`
Create a screenshot of a page of the GitLab instance. \
The screenshot is saved as a PNG in the same directory as where your script is running.

Arguments:

- **url**: string (_required_) \
The URL of the GitLab page.
- **filename**: string (_optional_) \
The filename of the saved screenshot. \
If no name is given, a `yyyymmdd-hhmmss` datetime stamp is used.
N.B. this is a named argument, e.g. `screenshot 'http://... name: 'my screenshot'`

```ruby
# Create a screenshot of the Admin page
# Save it as <datetime stamp>.png
salt.screenshot 'http://localhost/admin'

# Create a screenshot of the Users page
# Save it as userspage.png
salt.screenshot 'http://localhost/admin/users', name: 'userspage'
```

### `time_machine`
Jump in time. \
If you would run this on your local machine it'll end in tears. \
SALT checks if you're running in a VM to prevent yourself from shooting in the foot.

Arguments:

- **datetime**: string (_required_) \
The date and/or time to jump to. \
You can specify a date plus time, only a date, or only a time. \
The full format is `yyyy-mm-dd hh:mm:ss`. \
If you leave out the time part, it defaults to midnight. \
If you leave out the seconds part, it defaults to the top of the hour.\

```ruby
salt.time_machine '2020-11-01'
salt.time_machine '2020-11-01 23:59'
```

### `time_machine_off`
If you traveled in time with `time_machine` and your script ends, the VM in which you're running the tests will remain stuck in time. \
Using `time_machine_off` you can jump back to the present and continue on this mortal coil.

If you forgot to do this and would like to pull your VM back to the present, you can manually run the `time_machine_off` script on the commandline.

Arguments:

- none

```ruby
salt.time_machine_off
```

### `pause`
Pause the script, so you can manually interact with the test instance, e.g. to view admin screens or inspect the database. \
Once you hit `Return` the test script will continue.

Arguments:

- none

```ruby
salt.pause
```

## Examples

### Minimal script:

```ruby
#!/usr/bin/env ruby

# Include SALT
require './app/salt'

# Initialize SALT
salt = Salt.new

# Start a GitLab 13.5.4 instance
salt.gitlab '13.5.4'

# Show details of all users (only root in a new instance)
salt.list_details_users
```
`chmod +x` this script and run it.

### Demonstrate license overage:
The script expects a `my-10-user-license.gitlab-license` license file to be present in the same directory as the script.

```ruby
#!/usr/bin/env ruby

require './app/salt'

salt = Salt.new
salt.gitlab '13.5.4'

# Load a license for 10 users
salt.add_license 'my-10-user-license.gitlab-license'

# Create 10 users
# There are 11 users, the 10 we created + root
salt.create_users '01'..'10'

# Make a screenshot of the Admin panel
salt.screenshot 'http://localhost/admin', name: 'screen 1'

# Run the historical maximum user counter task
salt.kick_muc

# Make a screenshot of the Admin panel
salt.screenshot 'http://localhost/admin', name: 'screen 2'

# Delete one user
salt.delete_user '10'

# Run the historical maximum user counter task
salt.kick_muc

# Make a screenshot of the Admin panel
salt.screenshot 'http://localhost/admin', name: 'screen 3'

# Stop & delete the Docker container
salt.stop
salt.delete
```

## Missing
Commands that might be useful, but I didn't need them yet:

- Create project
- Add user to project
- Change user role (admin, developer, guest, auditor)
- Delete license
- Create bots
- Create an issue (to test if instance is read-only)

